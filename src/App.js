import React, { useState } from 'react';

import { Input, Space, Row, Col, PageHeader, Menu, Layout, Dropdown, Button, Card, Divider } from 'antd';
import { DownOutlined, StarTwoTone } from '@ant-design/icons';
import {
  ShopOutlined
} from '@ant-design/icons';

import './App.css';

const { Search } = Input;
const { Header, Content, Footer, Sider } = Layout;

const sideBarMenu = [
    {
      key: 1,
      icon: <ShopOutlined/>,
      label: `Restaurants `,
    },
    {
      key: 2,
      icon: <ShopOutlined/>,
      label: `Cuisines `,
    },
    {
      key: 3,
      icon: <ShopOutlined/>,
      label: `Locations `,
    },
]

function convertMenuItem(object) {
    let key;
    let label;
    let arr = [];
    for(let i = 0; i < object.length; i++){
        key = object[i].Name; //i+1;
        label = object[i].Name;
        arr.push({"key": key, "label": label});
    }
    return arr;
}

export default function Restaurants() {

    const [dataSource, setDataSource] = useState([]);

    const [APIData, setAPIData] = useState([]);
    const [cuisineMenuArray, setCuisineMenuArray] = useState([]);

    let cuisineMenu = null;

    async function onSearch(value) {
        let response = await fetch(`https://uk.api.just-eat.io/restaurants/bypostcode/${value}`)
        let responseJson = await response.json();
        cuisineMenu = convertMenuItem(responseJson["MetaData"]["CuisineDetails"]);
        responseJson=responseJson["Restaurants"]
            .map(({Id, Name, RatingAverage, Cuisines, IsOpenNow})=>({Id, Name, RatingAverage, Cuisines, IsOpenNow}))
            .filter((element) => element.IsOpenNow===true)
        setDataSource(JSON.parse(JSON.stringify(responseJson)));
        setAPIData(JSON.parse(JSON.stringify(responseJson)));

        setCuisineMenuArray([{"key": 0, "label": "All"}, ...cuisineMenu]);

    };

    const searchItems = (searchValue) => {
        let filteredData = dataSource.filter((item)=> item.Name.toLowerCase().includes(searchValue.toLowerCase()))
        setAPIData(searchValue !== '' ? filteredData : dataSource)
    }

    const handleRatingClick = (ratingValue) => {
        let filteredData = dataSource.filter(item=> item.RatingAverage >= parseInt(ratingValue.key));
        setAPIData(ratingValue.key ==="0" ? dataSource : filteredData);
    }

    const handleCuisineClick = (cuisineValue) => {
        let cuisineResults = dataSource.filter(item => item.Cuisines.find(i=>i.Name===cuisineValue.key));
        setAPIData(cuisineValue.key==="0" ? dataSource : cuisineResults);
    }


    const menuRating = (
      <Menu
        onClick={(e) => handleRatingClick(e)}
        items={[
          {
            label: 'All',
            key: '0',
          },
          {
            label: '1 Star',
            key: '1',
          },
          {
            label: '2 Star',
            key: '2',
          },
          {
            label: '3 Star',
            key: '3',
          },
          {
            label: '4 Star',
            key: '4',
          },
          {
            label: '5 Star',
            key: '5',
          }
        ]}
      />
    );

    const menuCuisine = (
      <Menu
        onClick={(e) => handleCuisineClick(e)}
        items={cuisineMenuArray}
      />
    );


    return (

        <Layout hasSider>
            <Sider
              style={{
                overflow: 'auto',
                height: '100vh',
                position: 'fixed',
                left: 0,
                top: 0,
                bottom: 0,
              }}
            >
              <div className="logo" />
              <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']} items={sideBarMenu} />
            </Sider>
            <Layout
              className="site-layout"
              style={{
                marginLeft: 200,
              }}
            >
              <Header
                className="site-layout-background"
                style={{
                  padding: 0,
                }}
              />
                <PageHeader
                className="site-page-header"
                onBack={() => null}
                title="Restaurant Search"
              />
              <Content
                style={{
                  margin: '24px 16px 0',
                  overflow: 'initial',
                }}
              >
                <div
                  className="site-layout-background"
                  style={{
                    padding: 24,
                    textAlign: 'center',
                  }}
                >
                    <h2>Firstly, search postcode</h2>
                <Search placeholder="Search for postcode" onSearch={onSearch} enterButton />

                <Divider />
                <h3>Filters</h3>
                <Search placeholder="Search Name" style={{ width: 200 }} onChange={(e) => searchItems(e.target.value)} enterButton />
                <Dropdown overlay={menuRating}>
                  <Button>
                    <Space>
                      Min. Rating
                      <DownOutlined />
                    </Space>
                  </Button>
                </Dropdown>
                <Dropdown overlay={menuCuisine}>
                  <Button>
                    <Space>
                      Cuisine
                      <DownOutlined />
                    </Space>
                  </Button>
                </Dropdown>

                <Divider />


                <div className="site-card-border-less-wrapper">
                    <Row>
                    <Col xs={2} sm={4} md={6} lg={8} xl={10}>
                    </Col>
                    <Col xs={20} sm={16} md={12} lg={8} xl={4}>
                      {APIData.map((item) => {
                                return (
                                    <Card title={item.Name}
                                          key={item.Id+"A"}
                                          bordered={true}
                                          style={{ width: 300 }}
                                          cover={
                                                <div style={{ overflow: "hidden", height: "70px" }}>
                                                  <img
                                                    alt="example"
                                                    style={{ height: "100%" }}
                                                    src="https://i.ibb.co/BgTHRLr/Hnet-com-image-1-removebg-preview.png"
                                                  />
                                                </div>
                                              }
                                    >
                                      <p key={item.Id+"c"}><StarTwoTone twoToneColor="#Fbe55f" /> {item.RatingAverage}/6</p>
                                        { item.Cuisines.map( (c, i) => {return (<p key={i+"cu"}>{c.Name}</p>)}) }
                                    </Card>
                                )
                            })
                      }
                    </Col>
                    <Col xs={2} sm={4} md={6} lg={8} xl={10}>
                    </Col>
                  </Row>

                </div>


              </div>

              </Content>
              <Footer
                style={{
                  textAlign: 'center',
                }}
              >
                Ant Design ©2022
              </Footer>
            </Layout>
        </Layout>

    )
}