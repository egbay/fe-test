<div id="top"></div>


<!-- PROJECT LOGO -->
<br />
<div align="center">

  <h3 align="center">Restaurant Search Application v1</h3>


</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#prerequisites">User Story</a></li>
        <li><a href="#prerequisites">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#backlog">Backlog</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Restaurant search application built with React.JS as for a frontend technical test.
It connects to JustEat Public API to collect restaurant data.

### User Story
* Given I am a user running the application
* When I submit an outcode (e.g. SE19)
* Then I want to see a list of restaurants
* And I only want to see restaurants that are currently open

  
### Project Directory

  ```sh
  \---fe-test-egb
    |   .gitignore
    |   README.md
    |   package-lock.json
    |   package.json
    |   craco.config.js
    |   yarn.lock
    |   
    \---src
        +---components
        |       Home.js
        |       SearchResult.js
        |   App.css
        |   App.js
        |   App.test.js
        |   index.css
        |   index.js
        |   logo.svg
        |   reportWebVitals.js
        |   setupTests.js
        |       
    +---public
    |       favicon.ico
    |       index.html
    |       logo192.png
    |       logo512.png
    |       manifest.json
    |       robots.txt
    |
  ```



<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* ReactJS
* Ant Design


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started


You can find information about how you could set up project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

You need to install npm and node, if you don't have it please install it with the command below.
* npm
  ```sh
  npm install npm@8.5.1 -g
  ```
### Installation

_To install this project properly, please follow the installation steps below._

1. Clone this repository
   ```sh
   git clone https://github.com/egbay/fe-test.git
   cd fe-test/
   ```
2. Install NPM packages
   ```sh
   yarn install
   ```
7. Start application
    ```sh
   yarn start
   ```
8. Start test operation
    ```sh
   yarn test
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

1. Put a postcode in first search area and click search icon.
2. Wait for results to be loaded
3. Use filters for open restaurants
   - searching for restaurant names
   - minimum average ratings
   - food type

    
<p align="right">(<a href="#top">back to top</a>)</p>



<!-- BACKLOG -->
## Backlog
- [ ] Adding infinite scrolling and lazy loading.
- [ ] Using components for pages.
- [ ] Adding routing for separation of two pages (one for searching postcode, one for showing results with filters).
- [ ] Adding multiple filters.
- [ ] Fixing design issues.
- [ ] Code has some repetition. Refactoring is needed.


<p align="right">(<a href="#top">back to top</a>)</p>


