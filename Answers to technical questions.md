Technical questions
1. How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.
   - I've worked on two evenings about 3-4 hours each. Last ReactJS project that I've worked on was around 6 months ago. So I've felt that I've forgotten many things about framework. Most of the time I've tried to remember. This reminds me; If you don't practise, you'll lose your ability.
   - I would add following below:
     - [ ] Adding testing. (didn't have experience and have time to take a look)
     - [ ] Adding infinite scrolling and lazy loading.
     - [ ] Using components for pages.
     - [ ] Adding routing for separation of two pages (one for searching postcode, one for showing results with filters).
     - [ ] Adding multiple filters.
     - [ ] Fixing design issues.
   1. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.
      - I usually use Python but honestly I don't usually follow new upgrades. I'm not at that stage I think. I hope in near future I will follow and keep update myself with the details of language.
2. How would you track down a performance issue in production? Have you ever had to do this?
   - Mostly I develop backend and some small projects so I didn't have experience with frontend performance tracking on production. I even didn't have write test on frontend side. I would use monitoring and logging services like Sentry, automated testing tool like JMeter.  